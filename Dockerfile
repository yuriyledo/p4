FROM python:3.9.7
WORKDIR /code
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt --no-cache-dir
COPY . .
CMD ["flask", "run", "--host", "0.0.0.0"]
