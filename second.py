import requests
from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
    return f"second => {requests.get('http://app_third:5000').text}"
