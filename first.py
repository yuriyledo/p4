import requests
from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
    return f"first => {requests.get('http://app_second:5000').text}"